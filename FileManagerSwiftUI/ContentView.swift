//
//  ContentView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI
import Foundation
import GoogleSignIn

struct ContentView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @EnvironmentObject var authViewModel: AuthenticationViewModel
    
    var body: some View {
        NavigationView {
            if self.sheetDataViewModel.isLoading {
                LoadingView()
            }
            else {
                FileManagerItemsView().environmentObject(self.sheetDataViewModel)
                    .alert(isPresented: self.$authViewModel.showSignOutAlert) {
                        Alert(title: Text("Sign Out"),
                              message: Text("Do you really want to sign out?"),
                              primaryButton: .default(Text("Yes")) {
                                self.authViewModel.signOut()
                              },
                              secondaryButton: .cancel())
                    }
            }
        }.onAppear {
            self.sheetDataViewModel.getAllItemsSet()
        }
        .accentColor(.black)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(SheetDataViewModel.shared)
    }
}
