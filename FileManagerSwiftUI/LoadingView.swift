//
//  LoadingView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 09.06.2021.
//

import SwiftUI

struct LoadingView: View {
    @State private var rotating = false
    
    var body: some View {
        VStack {
            Circle()
                .trim(from: 0, to: 0.7)
                .stroke(Color(.darkGray), lineWidth: 5)
                .frame(width: 100, height: 100)
                .rotationEffect(Angle(degrees: rotating ? 360 : 0))
                .animation(Animation.default.repeatForever(autoreverses: false), value: self.rotating)
                .transition(.opacity)
                .onAppear {
                    self.rotating = true
                }
        }.animation(.default)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}

