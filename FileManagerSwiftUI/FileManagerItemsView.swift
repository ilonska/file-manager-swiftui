//
//  FileItemsView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI

struct FileManagerItemsView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    var openedFolder: FileManagerItem?
    
    // Can't update the currently opened folder in ViewModel
    // because EnvironmentObject can't be changed in init
    // So have to use a workaround
    
    init(parentFolder: FileManagerItem? = nil) {
        self.openedFolder = parentFolder
    }
    
    var body: some View {
        if self.sheetDataViewModel.isGrid {
            GridView(openedFolder: self.openedFolder)
                .sheet(isPresented: self.$sheetDataViewModel.showItemTitleEditingView) {
                    ItemCreationView(parentFolder: self.openedFolder).environmentObject(self.sheetDataViewModel)
                }
        }
        else {
            ListView(openedFolder: self.openedFolder)
                .sheet(isPresented: self.$sheetDataViewModel.showItemTitleEditingView) {
                    ItemCreationView(parentFolder: self.openedFolder).environmentObject(self.sheetDataViewModel)
                }
        }
    }
}
