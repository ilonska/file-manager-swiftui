//
//  GridView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI

struct GridView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @EnvironmentObject var authViewModel: AuthenticationViewModel
    @State var showDeletionAlert: Bool = false
    var grids: [GridItem] = .init(repeating: GridItem(), count: 3)
    var openedFolder: FileManagerItem?
    init(openedFolder: FileManagerItem? = nil) {
        self.openedFolder = openedFolder
    }
    
    var body: some View {
        if self.sheetDataViewModel.folderContentsArray(parentFolder: self.openedFolder).isEmpty {
            EmptyView()
                .navigationTitle(self.sheetDataViewModel.navigationBarTitle(for: self.openedFolder))
        }
        else {
            ScrollView(.vertical) {
                LazyVGrid(columns: self.grids, alignment: .center, spacing: 30) {
                    ForEach(self.sheetDataViewModel.folderContentsArray(parentFolder: self.openedFolder), id: \.id) { item in
                        VStack(alignment: .center) {
                            if item.type == .folder {
                                NavigationLink(
                                    destination: FileManagerItemsView(parentFolder: item),
                                    label: {
                                        VStack {
                                            GridViewCell(sheetDataViewModel: self._sheetDataViewModel, item: item)
                                        }
                                    })
                            }
                            else {
                                GridViewCell(sheetDataViewModel: self._sheetDataViewModel, item: item)
                                    .onLongPressGesture { // Still couldn't make it work for navigation links
                                        self.showDeletionAlert.toggle()
                                    }
                                    .alert(isPresented: $showDeletionAlert) {
                                        Alert(title: Text("Confirm Deletion"),
                                              message: Text("Are you sure about deleting this item?"),
                                              primaryButton: .destructive(Text("Delete")) {
                                                if self.authViewModel.isSignedIn {
                                                    self.sheetDataViewModel.deleteItem(item)
                                                }
                                                else {
                                                    self.authViewModel.signIn()
                                                }
                                              },
                                              secondaryButton: .cancel())
                                    }
                            }
                        }
                    }
                }
            }.foregroundColor(.black)
            .navigationTitle(self.sheetDataViewModel.navigationBarTitle(for: self.openedFolder))
            .toolbar {
                AppToolbar(sheetDataViewModel: self._sheetDataViewModel, authViewModel: self._authViewModel)
            }
        }
    }
    
    struct GridViewCell: View {
        @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
        @State var showDeletionAlert: Bool = false
        var item: FileManagerItem
        var body: some View {
            VStack {
                Image(systemName: item.symbolName)
                    .resizable()
                    .frame(width: 70, height: 70, alignment: .center)
                    .cornerRadius(5)
                    .scaledToFit()
                Text(item.name)
                    .truncationMode(.middle)
                    .scaledToFit()
            }
        }
    }
    
    struct GridView_Previews: PreviewProvider {
        static var previews: some View {
            GridView().environmentObject(SheetDataViewModel.shared)
        }
    }
}
