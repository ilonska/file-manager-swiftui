//
//  AppToolbar.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 08.06.2021.
//

import SwiftUI

struct AppToolbar: ToolbarContent {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @EnvironmentObject var authViewModel: AuthenticationViewModel
    
    var body: some ToolbarContent {
        ToolbarItemGroup(placement: .navigationBarLeading) {
            Button(action: { self.authViewModel.isSignedIn ? self.authViewModel.showSignOutAlert.toggle() : self.authViewModel.signIn() }, label: { Image(systemName: self.authViewModel.isSignedIn ? "person.circle.fill" : "person.circle") }).foregroundColor(.black)
            Button(action: { self.sheetDataViewModel.isGrid.toggle() }, label: { Image(systemName: self.sheetDataViewModel.isGrid ? "list.dash" : "rectangle.grid.2x2") }).foregroundColor(.black)
        }
        
        ToolbarItem(placement: .navigationBarTrailing) {
            Button(action: {
                if self.authViewModel.isSignedIn {
                    self.sheetDataViewModel.showItemTitleEditingView.toggle()
                }
                else {
                    self.authViewModel.signIn()
                }
            }, label: { Image(systemName: "plus") }).foregroundColor(.black)
        }
    }
}
