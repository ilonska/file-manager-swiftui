//
//  ItemCreationScreen.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI

struct ItemCreationView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @Environment(\.presentationMode) private var presentation
    @State private var newItem: FileManagerItem = FileManagerItem(name: "", type: "")
    @State private var showingBlankNameAlert = false
    @State private var savingNewItemIsImpossible = false
    private var parentFolder: FileManagerItem?
    
    init(parentFolder: FileManagerItem?) {
        if let parentFolder = parentFolder {
            self.parentFolder = parentFolder
            self.newItem.parent = parentFolder.id
        }
        self.newItem.type = .file
    }
    
    var body: some View {
        VStack() {
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/) {
                Text("Pick item type")
                    .fontWeight(.light)
                    .padding(15)
                FileTypeButtonsView(currentType: self.newItem.type, item: self.newItem)
            }
            
            Text("Enter a name for item:")
                .font(.title)
            TextField("Name", text: $newItem.name)
                .padding(.vertical, 10)
                .padding(.horizontal, 10)
                .overlay(Rectangle().frame(height: 2).padding(.top, 35))
                .foregroundColor(Color(.black))
                .padding(10)
            
            HStack(alignment: .center, spacing: 20) {
                Button(action: {
                    presentation.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
                        .fontWeight(.light)
                        .padding()
                        .background(Color(.systemGray))
                        .cornerRadius(5)
                        .foregroundColor(.white)
                }
                
                Button(action: {
                    if self.newItem.name == "" {
                        self.showingBlankNameAlert = true
                    }
                    else {
                        self.sheetDataViewModel.addNewItem(self.newItem) { completed in
                            if completed {
                                presentation.wrappedValue.dismiss()
                            }
                            else {
                                self.savingNewItemIsImpossible = true
                            }
                        }
                    }
                }) {
                    Text("Save")
                        .fontWeight(.light)
                        .padding()
                        .background(Color(.red))
                        .cornerRadius(5)
                        .foregroundColor(.white)
                }
            }.fixedSize(horizontal: true, vertical: true)
            .alert(isPresented: self.$showingBlankNameAlert) {
                Alert(title: Text("Item name can't be left blank"), message: Text("Fill in a name field"), dismissButton: .default(Text("Got it!")))
            }
            .alert(isPresented: self.$savingNewItemIsImpossible) {
                Alert(title: Text("Item with such name already exists"), message: Text("\(self.newItem.name) \(String(describing: self.newItem.type)) already exists in \(self.sheetDataViewModel.navigationBarTitle(for: self.parentFolder)) folder"), dismissButton: .default(Text("OK")))
            }
        }.accentColor(.black)
    }
}

struct ItemCreationScreen_Previews: PreviewProvider {
    static var previews: some View {
        ItemCreationView(parentFolder: nil)
    }
}

struct FileTypeButton: View {
    @Binding var currentlySelectedType: FileManagerItem.AppFileType
    var type: FileManagerItem.AppFileType
    var item: FileManagerItem
    
    var body: some View {
        Button(action: {
            self.currentlySelectedType = self.type
            self.item.type = self.currentlySelectedType
        }) {
            VStack {
                Image(systemName: self.type == .folder ? "folder" : "doc.richtext")
                    .font(.system(size: 60))
                Text(self.type == .folder ? "Folder" : "File").fontWeight(.light)
            }
        }.foregroundColor(self.currentlySelectedType == self.type ? Color.red : Color.black)
    }
}

struct FileTypeButtonsView: View {
    @State var currentType: FileManagerItem.AppFileType
    @State var item: FileManagerItem
    
    var body: some View {
        HStack(alignment: .center, spacing: 20) {
            FileTypeButton(currentlySelectedType: $currentType, type: .file, item: self.item)
            FileTypeButton(currentlySelectedType: $currentType, type: .folder, item: self.item)
        }
    }
}

