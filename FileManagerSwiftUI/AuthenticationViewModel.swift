//
//  FileManagerSwiftUIApp.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 02.06.2021.
//

import Foundation
import Firebase
import GoogleSignIn

class AuthenticationViewModel: NSObject, ObservableObject {
    @Published var isSignedIn: Bool = false
    @Published var showSignInAlert: Bool = false
    @Published var showSignOutAlert: Bool = false
    
    override init() {
        super.init()
        
        self.setupGoogleSignIn()
        self.restorePreviousSignIn()
    }
    
    func signIn() {
        if GIDSignIn.sharedInstance().currentUser == nil {
            GIDSignIn.sharedInstance().presentingViewController = UIApplication.shared.windows.first?.rootViewController
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func signOut() {
        GIDSignIn.sharedInstance().signOut()
        do {
            try Auth.auth().signOut()
            isSignedIn = false
        } catch let signOutError as NSError {
            assertionFailure("Failed to sign out: \(signOutError.localizedDescription)")
        }
    }
    
    func restorePreviousSignIn() {
        guard !GIDSignIn.sharedInstance().hasPreviousSignIn() else {
            GIDSignIn.sharedInstance().restorePreviousSignIn()
            return
        }
    }
    
    private func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().delegate = self
    }
}

extension AuthenticationViewModel: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            self.firebaseAuthentication(withUser: user)
        } else {
            print(error.debugDescription)
        }
    }
    
    private func firebaseAuthentication(withUser user: GIDGoogleUser) {
        if let authentication = user.authentication {
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
            
            Auth.auth().signIn(with: credential) { (_, error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    self.isSignedIn = true
                }
            }
        }
    }
}
