//
//  ListView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI

struct ListView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @EnvironmentObject var authViewModel: AuthenticationViewModel
    var openedFolder: FileManagerItem?
    init(openedFolder: FileManagerItem? = nil) {
        self.openedFolder = openedFolder
    }
    
    var body: some View {
        if self.sheetDataViewModel.folderContentsArray(parentFolder: self.openedFolder).isEmpty {
            EmptyView(openedFolder: self.openedFolder)
                .navigationTitle(self.sheetDataViewModel.navigationBarTitle(for: self.openedFolder))
        }
        else {
            List {
                ForEach(self.sheetDataViewModel.folderContentsArray(parentFolder: self.openedFolder), id: \.id) { item in
                    if item.type == .folder {
                        NavigationLink(
                            destination: FileManagerItemsView(parentFolder: item),
                            label: {
                                VStack {
                                    ListCell(item: item)
                                }
                            })
                    }
                    else {
                        HStack {
                            ListCell(item: item)
                        }
                    }
                    
                }.onDelete(perform: self.delete)
            }.foregroundColor(.black)
            .navigationTitle(self.sheetDataViewModel.navigationBarTitle(for: self.openedFolder))
            .toolbar {
                AppToolbar(sheetDataViewModel: self._sheetDataViewModel, authViewModel: self._authViewModel)
            }
        }
    }
    
    private func delete(at indexSet: IndexSet) {
        if self.authViewModel.isSignedIn {
            let displayedItemsArray = self.sheetDataViewModel.folderContentsArray(parentFolder: self.openedFolder)
            indexSet.forEach { indexSet in
                let itemToRemove = displayedItemsArray[indexSet]
                self.sheetDataViewModel.deleteItem(itemToRemove)
            }
        }
        else {
            self.authViewModel.signIn()
        }
    }
}

struct ListCell: View {
    var item: FileManagerItem
    var body: some View {
        HStack {
            Image(systemName: self.item.symbolName)
                .resizable()
                .frame(width: 30, height: 30)
            Text(item.name)
        }.padding(.vertical)
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView().environmentObject(SheetDataViewModel.shared)
    }
}
