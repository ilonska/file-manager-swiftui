//
//  EmptyView.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 01.06.2021.
//

import SwiftUI

struct EmptyView: View {
    @EnvironmentObject var sheetDataViewModel: SheetDataViewModel
    @EnvironmentObject var authViewModel: AuthenticationViewModel
    var openedFolder: FileManagerItem?
    init(openedFolder: FileManagerItem? = nil) {
        self.openedFolder = openedFolder
    }
    
    var body: some View {
        VStack(alignment: .center) {
            Image(systemName: "folder")
                .font(.system(size: 100))
            Text("Folder is empty")
                .fontWeight(.light)
                .padding(10)
                .font(.system(size: 25))
        }
        .padding()
        .scaledToFill()
        .toolbar {
            AppToolbar(sheetDataViewModel: self._sheetDataViewModel, authViewModel: self._authViewModel)
        }
    }
}

struct EmptyView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
