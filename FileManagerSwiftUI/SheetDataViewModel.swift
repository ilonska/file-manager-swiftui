//
//  SheetsDataManager.swift
//  FileManager
//
//  Created by Ilona Demkowska on 25.05.2021.
//

import Foundation

class SheetDataViewModel: ObservableObject {
    static let shared = SheetDataViewModel()
    @Published var fileManagerItemsStore = Set<FileManagerItem>()
    @Published var isGrid: Bool = false
    @Published var showItemTitleEditingView: Bool = false
    @Published var isLoading: Bool = true
    
    func navigationBarTitle(for folder: FileManagerItem? = nil) -> String {
        return folder?.name ?? "Documents"
    }
    
    func getAllItemsSet() {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            GoogleSheetsService.shared.fetchSpreadsheetRowsArray { result in
                switch result {
                case .failure(let error):
                    assertionFailure("Failed to fetch values from spreadsheet rows data: \(error.localizedDescription)")
                case .success(let rows):
                    for row in rows {
                        assert(row.count == 4, "Incorrect number of fetched columns")
                        let item = FileManagerItem(name: row[3], type: row[2], parent: UUID(uuidString: row[1]), id: UUID(uuidString: row[0]))
                        
                        DispatchQueue.main.async { [weak self] in
                            self?.fileManagerItemsStore.insert(item)
                            self?.isLoading = false
                        }
                    }
                }
            }
        }
    }
    
    // Need it to be ordered to correspond with cells in List
    func folderContentsArray(parentFolder: FileManagerItem? = nil) -> [FileManagerItem] {
        var itemsSet: Set<FileManagerItem>
        if let parentFolder = parentFolder {
            itemsSet = Set(self.childrenOf(parentFolder))
        }
        else {
            itemsSet = Set(self.fileManagerItemsStore.filter( { $0.isInRoot }))
        }
        // Sort like this: folders first and in alphabetical order
        let array = itemsSet.sorted(by: { ($0.type.rawValue, $0.name.lowercased()) < ($1.type.rawValue, $1.name.lowercased()) })
        return array
    }
    
    func addNewItem(_ item: FileManagerItem, completion: ((Bool) -> Void)) {
        if self.fileManagerItemsStore.contains(where: { $0.name == item.name && $0.parent == item.parent && $0.type == item.type }) {
            completion(false)
        }
        else {
            self.fileManagerItemsStore.insert(item)
            self.updateSpreadsheet()
            completion(true)
        }
    }
    
    func deleteItem(_ item: FileManagerItem) {
        self.fileManagerItemsStore.remove(item)
        self.fileManagerItemsStore.subtract((self.childrenOf(item)))
        self.updateSpreadsheet()
    }
    
    private func childrenOf(_ item: FileManagerItem) -> Set<FileManagerItem> {
        return self.fileManagerItemsStore.filter { $0.parentEquals(to: item.id) }
    }
    
    private func findItem(withID id: UUID) -> FileManagerItem? {
        return self.fileManagerItemsStore.first(where: { $0.id == id })
    }
    
    private func updateSpreadsheet() {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            GoogleSheetsService.shared.postDataToSpreadSheet(newValues: self.convertedStoreValues) { result in
                switch result {
                case .success(let successfulResult):
                    print("Data uploaded to spreadsheet: \(successfulResult)")
                case .failure(let failedResult):
                    assertionFailure("Failed to upload data to spreadsheet: \(failedResult)")
                }
            }
        }
    }
    
    private var convertedStoreValues: [[String]] {
        var valuesArray = [[String]]()
        self.fileManagerItemsStore.forEach { item in
            valuesArray.append(self.convertToSpreadSheetRow(item: item))
        }
        return valuesArray
    }
    
    private func convertToSpreadSheetRow(item: FileManagerItem) -> [String] {
        return [item.id.uuidString, item.parent?.uuidString ?? "", item.type.rawValue, item.name]
    }
}
