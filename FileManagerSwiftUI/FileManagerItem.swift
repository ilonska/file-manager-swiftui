//
//  FileManagerItem.swift
//  FileManager
//
//  Created by Ilona Demkowska on 26.05.2021.
//

import Foundation
import SwiftUI

class FileManagerItem: Identifiable, ObservableObject {
    
    @Published var id: UUID
    @Published var name: String
    @Published var type: AppFileType
    @Published var parent: UUID?
    @Published var children: [FileManagerItem] = []
    
    enum AppFileType: String {
        case folder = "d"
        case file = "f"
        case unknown = "?"
    }
    
    init(name: String, type: String, parent: UUID? = nil, id: UUID? = nil) {
        self.name = name
        self.parent = parent
        self.id = id ?? UUID()
        
        if let convertedType = AppFileType(rawValue: type.lowercased()) {
            self.type = convertedType
        }
        else {
            self.type = .unknown
        }
    }
    
    var symbolName: String {
        switch self.type {
        case .file:
            return "doc.richtext"
        case .folder:
            return "folder"
        default:
            return "questionmark.square.fill"
        }
    }
    
    var isInRoot: Bool { return self.parent == nil }
    
    func parentEquals(to UUID: UUID) -> Bool {
        return self.parent == UUID
    }
}

extension FileManagerItem: Hashable {
    static func == (lhs: FileManagerItem, rhs: FileManagerItem) -> Bool {
        return (lhs.name == rhs.name &&
        lhs.id == rhs.id &&
        lhs.parent == rhs.parent &&
        lhs.type == lhs.type &&
        lhs.children == rhs.children)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
        hasher.combine(self.id)
    }
}
