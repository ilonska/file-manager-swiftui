//
//  FileManagerSwiftUIApp.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 31.05.2021.
//

import SwiftUI
import Firebase
import GoogleSignIn

@main
struct FileManagerSwiftUIApp: App {
    @StateObject var sheetDataViewModel = SheetDataViewModel()
    @StateObject var authViewModel = AuthenticationViewModel()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(self.sheetDataViewModel)
                .environmentObject(self.authViewModel)
        }
    }
    
    init() {
        self.setupAuthentication()
    }
    
    private func setupAuthentication() {
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().scopes = GoogleSheetsService.shared.scopes
    }
}
