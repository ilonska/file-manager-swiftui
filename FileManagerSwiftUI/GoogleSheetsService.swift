//
//  GoogleSheetsService.swift
//  FileManagerSwiftUI
//
//  Created by Ilona Demkowska on 07.06.2021.
//

import Foundation
import GoogleSignIn
import GTMSessionFetcher
import GoogleAPIClientForREST
import Firebase

// https://developers.google.com/sheets/api/samples/reading
// https://developers.google.com/sheets/api/samples/writing
// https://stackoverflow.com/a/52413833/15710852

//Test account:
//swiftuifilemanager@gmail.com
//testapp11

class GoogleSheetsService {
    static let shared = GoogleSheetsService()
    private lazy var service = GTLRSheetsService()
    private lazy var spreadsheetID = "1vTglrNsnDYZf248RImKWqv24ZzbzkiGhVSJSefHCO4s"
    private lazy var apiKey = FirebaseApp.app()?.options.apiKey
    let scopes = [kGTLRAuthScopeSheetsSpreadsheets] // See, edit, create, and delete
    private lazy var range = "Sheet1!A1:D" // Sheet titled "Sheet1", range A1 up to the last row in D column
    
    func fetchSpreadsheetRowsArray(completion: @escaping (Result<[[String]], Error>) -> Void) {
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: self.spreadsheetID,
                                                                range: self.range)
        self.service.apiKey = apiKey // Utilizing API key here to access the sheet data without signing in
        self.service.executeQuery(query) { ticket, range, error in
            if let error = error {
                assertionFailure(error.localizedDescription)
            }
            if let range = range as? GTLRSheets_ValueRange {
                if let values = range.values as? [[String]] {
                    completion(.success(values))
                } else if range.values == nil {
                    completion(.success([[String]()]))
                }
            }
        }
    }
    
    func postDataToSpreadSheet(newValues: [[String]], completion: @escaping (Result<String, Error>) -> Void) {
        let valueRange = GTLRSheets_ValueRange() // Holds the updated values and other params
        valueRange.majorDimension = "ROWS"
        valueRange.range = self.range
        valueRange.values = newValues
        
        GIDSignIn.sharedInstance().scopes = self.scopes
        // Posting data is possible only for authorized users
        self.service.authorizer = GIDSignIn.sharedInstance().currentUser.authentication.fetcherAuthorizer()
        let query = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange,
                                                                   spreadsheetId: self.spreadsheetID,
                                                                   range: self.range)
        query.valueInputOption = "USER_ENTERED"
        self.service.executeQuery(query) { ticket, object , error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            else {
                completion(.success(ticket.description))
            }
        }
    }
}
